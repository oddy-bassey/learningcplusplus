#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

/*
 * Definition
 * Prototype
 * Parameters and pass-by-value
 * return statement
 * default parameter values
 * overloading
 * passing arrays to function
 * pass-by-reference
 * inline functions
 * auto return type
 * recursive functions
 */
int main() {
    int random_number {};
    size_t count{10}; // number of random numbers to generate
    int min{1}; // lower bound
    int max{6}; // upper bound

    // seed the random number generator else you'll get the same sequence of random numbers every run
    cout << "RAND MAX on my system is: " << RAND_MAX << endl;
    srand(time(nullptr));

    for (size_t i{1}; i<=count; ++i) {
        random_number = (rand() % max) + min; // generate a random number [min, max]
        cout << "number generated: " << random_number << endl;
    }

    return 0;
}
