#include <iostream>

using namespace std;

void conditional_statements();
void iteration_statements();

int main() {
    //conditional_statements();
    iteration_statements();

    std::cout << "Hello, World!" << std::endl;
    return 0;
}

void conditional_statements() {
    // if , if-else and compound if-else statement
    int age;
    bool has_driving_license;

    cout << "What is your age:";
    cin >> age;
    cout << endl << "Do you have a driving license { 0 - false, 1 - true}):";
    cin >> has_driving_license;

    if (age >= 15) cout << "you are old enough to drive a car!";

    if (age-3 >= 15)
        cout << "you certainly can drive a car!"<< endl;
    else
        cout << "you're not allowed to drive a car!"<< endl;

    if (age-3 >= 15)
        cout << "you are allowed to drive a car!"<< endl;
    else
        cout << "you should not drive a car!"<< endl;

    if (age >= 15) {
        if (has_driving_license) {
            cout << "Go ahead and drive a car!"<< endl;
        }else {
            cout << "You can't drive a car because you don't have a driving license!"<< endl;
        }
    }else {
        cout << "you are not allowed to drive a car!"<< endl;
    }
}

void iteration_statements() {

    // for loop
    cout<<"For loop"<<endl;
    cout<<"printing I"<<endl;
    for(int i=0; i<10; i++) {
        cout << "i = "<< i<< endl;
    }

    cout<<"printing (I * J)"<<endl;
    for(int i=0, j=0; i<10; i++, j++) {
        cout << i << " * "<< j << " = " << i*j<< endl;
    }

    // range-based for loop
    cout<<"Range-based for loop"<<endl;
    cout<<"printing vector"<<endl;
    for(int i : {56,45,23,63,24,657,998,342}) {
        cout << "i = "<< i<< endl;
    }

    // while loop
    cout<<"While loop"<<endl;
    int value {10};
    while (value >= 1) {
        cout << "value: "<< value << endl;
        value--;
    }

    // while loop
    cout<<"Do While loop"<<endl;
    do {
        value++;
        cout << "value: "<< value << endl;
    }while (value<10);
}