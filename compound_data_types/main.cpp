#include <iostream>
#include <vector>

using namespace std;

/*
 * - Arrays
 * - Multidimensional Arrays
 * - Vectors
 */
int main() {
    // Arrays
    cout<<"Working with Arrays"<< endl;
    char vowels[] {'a', 'e', 'i', 'o', 'u'};
    cout<<"vowel at index 3 is '"<<vowels[2]<<"' ...."<<endl;

    int scores[] {32, 45, 32, 56, 76};
    int scores_length = sizeof scores / sizeof scores[0];

    cout<<"score at index 4: "<<scores[3]<<endl;
    cout<<"scores array is stored at "<<scores<<" memory location!"<<endl;
    cout << "length of scores array : " << scores_length;

    // Multidimensional Arrays
    int movie_ratings[3][4] = {{0}};
    cout << "movie ratings for the month: " << endl;
    for (int i=0; i<3; i++) {
        for (int j=0; j<4; j++) {
            cout<<movie_ratings[i][j]<<" ";
        }
        cout<<endl;
    }

    // Vectors
    vector<int> test_scores {12, 34, 23, 43, 54};
    cout<<"last test score is: "<<test_scores.at(test_scores.size()-1)<<endl;
    test_scores.push_back(69);
    cout<<"last test score is: "<<test_scores[test_scores.size()-1]<<endl;

    int score {0};
    cout<<"Pls enter a new test score: ";
    cin>>score;
    test_scores.push_back(score);
    cout<<"last test score is: "<<test_scores.at(test_scores.size()-1)<<endl;

    // Multidimensional Vectors
    vector<vector<int>> song_ratings {
        {1,2,2,3},
        {4,4,3,5},
        {3,4,5,2},
        {2,2,5,4}
    };
    cout << "song ratings for the month: " << endl;
    song_ratings.push_back({2,4,3,4});

    for (auto & song_rating : song_ratings) {
        for (int j=0; j<song_rating.size(); j++) {
            cout<<song_rating.at(j)<<" ";
        }
        cout<<endl;
    }
    int data = 21 >> 1;
    cout <<"bitwise right shift of 21 is; "<<data;

    return 0;
}
