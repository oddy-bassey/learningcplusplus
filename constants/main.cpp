
// Created by oddyb on 09/09/2023.
//
#include <iostream>
#include <climits>

using namespace std;

/* Types of Constants in C++
 * Literal Constants
 *
 * Declared Constants
 *  - const keyword
 *
 * Constant Expressions
 *  - constexpr keyword
 *
 * Enumerated Constants
 *  - enum keyword
 *
 * Defined Constants
 *  - #define
 */
void declared_constants();
double calculate_room_cost(double price_per_room, int number_of_rooms);
double calculate_tax(double tax_rate, double room_cost);
double calculate_total_estimate(double room_cost, double tax);

int main() {
    declared_constants();

    return 0;
}

void declared_constants() {
    const double price_per_room {30};
    const double tax_rate {0.06};
    const int estimate_expiry {30};
    int number_of_rooms {0};
    cout<<"How many rooms would you like cleaned? ";
    cin >> number_of_rooms;

    const double room_cost = calculate_room_cost(price_per_room, number_of_rooms);
    const double tax = calculate_tax(tax_rate, room_cost);
    const double total_estimate = calculate_total_estimate(room_cost, tax);

    cout<<"======================================="<<endl;
    cout << "Price per room " << price_per_room << endl;
    cout << "Service cost for cleaning " << number_of_rooms << " rooms." << endl;
    cout << "cost: $" << room_cost << endl;
    cout<<"tax: $"<<tax<<endl;
    cout<<"======================================="<<endl;
    cout << "total estimate: $" << total_estimate << endl;
    cout<<"estimate is valid for "<< estimate_expiry <<"days"<<endl;
    cout<<"======================================="<<endl;
}

double calculate_room_cost(double price_per_room, int number_of_rooms) {
    return price_per_room * number_of_rooms;
}

double calculate_tax(double tax_rate, double room_cost) {
    return tax_rate * room_cost;
}

double calculate_total_estimate(double room_cost, double tax) {
    return room_cost + tax;
}