#include <iostream>
#include <cstring> // for c-style string functions
#include <cctype> // for character-based functions
#include <string> // C++ strings
#include <iomanip>
#include <vector>

using namespace std;

/*
 * Character functions
 * C-style strings
 * Working with C-style strings
 * C++ Strings
 * Working with C++ Strings
 *
 * Note: you should be using C++ Strings in modern C++
 */

void c_style_strings();
void c_plus_plus_strings();
void assignment_letter_pyramid();
void assignment_letter_pyramid_solution();

int main() {
//  c_style_strings();
//  c_plus_plus_strings();
//  assignment_letter_pyramid();
    assignment_letter_pyramid_solution();

    return 0;
}

void c_style_strings() {
    cout << "Character Strings!" << endl;
    char first_name[20] {};
    char last_name[20] {};
    char middle_name[20] {};
    char full_name[50] {};

    char temp[50] {};
    /*
    cout<< "enter your first name: " << endl;
    cin>> first_name;

    cout<< "enter your last name: " << endl;
    cin>> last_name;

    cout<< "Hello " << first_name << ", your firstname has "<< ::strlen(first_name) << " characters." << endl;
    cout<< "and your lastname has " << last_name << ", has "<< ::strlen(last_name) << " characters." << endl;

    ::strcpy(full_name, first_name);
    ::strcat(full_name, " ");
    ::strcat(full_name, last_name);

    cout << "your fullname is " << full_name << endl;
    */
    cout << "Enter your middlename: ";
    cin.getline(middle_name, 20);
    ::strcpy(temp, middle_name);

    if(::strcmp(middle_name, temp) == 0)
        cout << middle_name << " and " << temp << " are the same!" << endl;
    else
        cout << middle_name << " and " << temp << " are different!" << endl;

    for(int i=0; i< ::strlen(middle_name); i++) {
        if(isalpha(middle_name[i])) {
            middle_name[i] = ::toupper(middle_name[i]);
        }
    }
    cout<< "middle name: "<< middle_name << endl;
    if(::strcmp(middle_name, temp) == 0)
        cout << middle_name << " and " << temp << " are the same!" << endl;
    else
        cout << middle_name << " and " << temp << " are different!" << endl;
}

void c_plus_plus_strings() {
    string s0;
    string s1 {"Apple"};
    string s2 {"Banana"};
    string s3 {"Kiwi"};
    string s4 {"apple"};
    string s5 {s1};
    string s6 {s1, 0, 3};
    string s7 (10, 'X');

    cout << "s0: " << s0 << endl;
    cout << "s0 length: " << s0.length() << endl;
    cout << endl;

    // Initialization
    cout << "Initialization" << "\n---------------------" << endl;
    cout << "s1 is initialized to: " << s1 << endl;
    cout << "s2 is initialized to: " << s2 << endl;
    cout << "s3 is initialized to: " << s3 << endl;
    cout << "s4 is initialized to: " << s4 << endl;
    cout << "s5 is initialized to: " << s5 << endl;
    cout << "s6 is initialized to: " << s6 << endl;
    cout << "s7 is initialized to: " << s7 << endl;
    cout << endl;
    // Comparing
    cout << "Comparison" << "\n---------------------" << endl;
    cout << boolalpha;
    cout << s1 << " == " << s2 << " : " << (s1 == s2) << endl;
    cout << s1 << " > " << s2 << " : " << (s1 > s2) << endl;
    cout << s1 << " == " << s5 << " : " << (s1 == s5) << endl;
    cout << s2 << " == " << s1 << " : " << (s2 > s1) << endl;
    cout << s4 << " > " << s5 << " : " << (s4 > s5) << endl;
    cout << endl;

    // Assignment
    cout << "Assignment" << "\n---------------------" << endl;
    s1 = "Watermelon";
    cout << "s1 is now: " << s1 << endl;
    s2 = s1;
    cout << "s2 is now: " << s2 << endl;

    s3 = "Frank";
    cout << "s3 is now: " << s3 << endl;
    s3[0] = 'C';
    cout << "s3 changed first letter to C : " << s3 << endl;
    s3.at(0) = 'P';
    cout << "s3 changed first letter to P : " << s3 << endl;
    cout << endl;

    // Concatenation
    cout << "Concatenation" << "\n---------------------" << endl;
    s3 = "Watermelon";
    s3 = s5 + " and " + s2 + " juice";
    cout << "s3 is now: " << s3 << endl;
    cout << endl;

    // s3 = "Orange" + " and " + s2 + " juice"; // Compiler errror:

    // Looping
    cout << "Looping" << "\n---------------------" << endl;
    for(size_t i=0; i<s1.length(); i++)
        cout << s1.at(i);
    cout << endl;

    // range-based for loop
    for(char c : s3)
        cout << c;
    cout << endl;

    // Substring
    cout << "Substring" << "\n---------------------" << endl;
    s1 = "This is a test";
    cout << s1.substr(0, 4) << endl;
    cout << s1.substr(5, 2) << endl;
    cout << s1.substr(10, 4) << endl;

    // Erase
    cout << "Erase" << "\n---------------------" << endl;
    s1.erase(0, 5);
    cout << "s1 is now: " << s1 << endl;
    cout << endl;

    // getline
    cout << "getline" << "\n---------------------" << endl;
    string fullname {};
    cout << "Enter your fullname: ";
    getline(cin, fullname);
    cout << "Your fullname is: " << fullname << endl;
    cout << endl;\

    // find
    cout << "find" << "\n---------------------" << endl;
    s1 = "The secret word is love";
    string word {};
    cout << "Enter the word to find: ";
    cin >> word;

    size_t position = s1.find(word);
    if (position != string::npos)
        cout << "Found "<< word << " at position: " << position << endl;
    else
        cout << "Sorry " << word << " not found!" << endl;
}

void assignment_letter_pyramid() {
    string value {};
    vector<char> queue {};

    cout << "Enter any string of characters: " << endl;
    getline(cin, value);

    for(int i=0; i<value.length(); i++) {
        queue.push_back(value.at(i));

        for (char character: queue)
            cout << character;

        for(int j=i-1; j>=0; j--) {
            cout << value.at(j);
        }
        cout << endl;
    }
}

void assignment_letter_pyramid_solution() {

    // Instructor solution
    std::string letters{};

    std::cout << "Enter a string of letters so I can create a Letter Pyramid from it: ";
    getline(std::cin, letters);

    size_t num_letters = letters.length();

    int position {0};

    // for each letter in the string
    for (char c: letters) {

        size_t num_spaces = num_letters - position;
        while (num_spaces > 0) {
            cout << " ";
            --num_spaces;
        }

        // Display in order up to the current character
        for (size_t j=0; j < position; j++) {
            cout << letters.at(j);
        }

        // Display the current 'center' character
        cout << c;

        // Display the remaining characters in reverse order
        for (int j=position-1; j >=0; --j) {
            // You can use this line to get rid of the size_t vs int warning if you want
            auto k = static_cast<size_t>(j);
            cout << letters.at(k);
        }

        cout << endl; // Don't forget the end line
        ++position;
    }
}
