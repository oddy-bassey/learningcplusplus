#include <iostream>
#include <climits>

using namespace std;

int main() {

    cout << "Size of information\n\n";

    cout << "char : " << sizeof(char) << " bytes" << endl;
    cout << "int : " << sizeof(int) << " bytes" << endl;
    cout << "double : " << sizeof(double) << " bytes" << endl;
    cout << "float : " << sizeof(float ) << " bytes" << endl;
    cout << "unsigned int : " << sizeof(unsigned int) << " bytes" << endl;
    cout << "short : " << sizeof(short) << " bytes" << endl;
    cout << "long : " << sizeof(long) << " bytes" << endl;
    cout << "long long : " << sizeof(long long) << " bytes" << endl;

    return 0;
}
